#pragma once

#ifndef ADD_VECTOR
#define ADD_VECTOR
#include <vector>
#endif

#ifndef ADD_PUBLICTYPES
#define ADD_PUBLICTYPES
#include "publictypes.h"
#endif




class BoxPixel {
	//private:
public:
	int left;
	int top;
	int right;
	int bottom;



	BoxPixel() {}

	BoxPixel(int left, int top, int right, int bottom) {
		this->left = left;
		this->top = top;
		this->right = right;
		this->bottom = bottom;
	}

	//public:
	std::string getDimensions() {
		std::string str;
		str.append("(");
		str.append(std::to_string(left));
		str.append(", ");
		str.append(std::to_string(top));
		str.append(", ");
		str.append(std::to_string(right));
		str.append(", ");
		str.append(std::to_string(bottom));
		str.append(")");
		return str;
	}


	void setBoxDimensions (int left, int top, int right, int bottom) {
		this->left = left;
		this->top = top;
		this->right = right;
		this->bottom = bottom;
	}
	
	/*
	void setBoxLeft(int left) {
		this->left = left;
	}

	void setBoxTop(int top) {
		this->top = top;
	}

	void setBoxRight(int right) {
		this->right = right;
	}

	void setBoxBottom(int bottom) {
		this->bottom = bottom;
	}
	*/
};




enum TextType {
	DATA,
	ROW_LABEL,
	COLUMN_LABEL
};





class BinarizedMap
{
private:
	short int** binary_map;
	int grid_columns;
	int grid_rows;
	int increment;


public:
	BinarizedMap(int rows, int columns, int increment)
	{
		grid_columns = columns;
		grid_rows = rows;
		this->increment = increment;
		binary_map = new short int*[grid_columns];
		for (int x = 0; x < grid_columns; ++x)
			binary_map[x] = new short int[grid_rows];

		for (int x = 0; x < grid_columns; x++)
			for (int y = 0; y < grid_rows; y++)
				binary_map[x][y] = -1;

		//std::cout << "Finished printing - " << grid_columns << "columns, " << grid_rows << " rows, sample 4,7: " << binary_map[4][7] << std::endl;

	}

	int getIncrement() { return increment; }
	int getGridColumns() { return grid_columns; }
	int getGridRows() { return grid_rows; }
	int getBinaryMapPoint(int x, int y) { return binary_map[x][y]; }


	void printMap() {
		for (int y = 0; y < grid_rows; y++)
		{
			for (int x = 0; x < grid_columns; x++)
				if (binary_map[x][y] == -1)
					std::cout << " ";
				else
					std::cout << binary_map[x][y];
			std::cout << std::endl;
		}

	}

	bool is_number(const std::string &str)
	{
		if (str.find('0') != std::string::npos ||
			str.find('1') != std::string::npos ||
			str.find('2') != std::string::npos ||
			str.find('3') != std::string::npos ||
			str.find('4') != std::string::npos ||
			str.find('5') != std::string::npos ||
			str.find('6') != std::string::npos ||
			str.find('7') != std::string::npos ||
			str.find('8') != std::string::npos ||
			str.find('9') != std::string::npos)/*||
											   str.find('-') != std::string::npos ||
											   str.find('(') != std::string::npos ||
											   str.find(')') != std::string::npos)*/
		{
			return true;
		}
		return false;
	}

	void load_binary_map(int output_size, std::vector <BoxPixel> boxes, std::vector <std::string> output) { //should be using typedefs ocr_output_t and ocr_box_t

		int increment = getIncrement();
		int grid_columns = getGridColumns();
		int grid_rows = getGridRows();


		//std::cout << "output size: " << output_size << ", grid_rows = " << grid_rows << ", grid_columns" << grid_columns << ", increment = " << increment << std::endl;

		for (int i = 0; i < output_size; i++)
		{
			for (int x = 0; x < grid_columns; x++)
			{
				//std::cout << "Is boxes[" << i << "] (left = " << boxes[i].left << ", right = " << boxes[i].right << ") between " << increment*(x) << " and " << increment*(x + 1) << "?";
				if (boxes[i].left < increment*(x + 1) && boxes[i].right >= increment*x)
				{
					//std::cout << "...yes" << std::endl;
					for (int y = 0; y < grid_rows; y++)
					{
						//std::cout << "boxes[" << i << "].top = " << boxes[i].top << ", boxes[" << i << "].bottom = " << boxes[i].bottom << std::endl;
						if (boxes[i].top < increment*(y + 1) && boxes[i].bottom >= increment*y)
						{
							//std::cout << "output[" << i << "] = " << output[i] << " @ x = " << x << ", y = " << y << std::endl;
							if (is_number(output[i]))
							{
								//std::cout << "*** output[" << i << "] = " << output[i] << " @ x = " << x << ", y = " << y << ", is true" << std::endl;
								binary_map[x][y] = 1;
							}
							else if (binary_map[x][y] == -1) //only if it was not already confirmed that there is a number in that cell
							{
								binary_map[x][y] = 0;
							}
						}
					}
				}
			}
		}
		//std::cout << "Finished printing - Output size: " << output_size << ", " << grid_columns << "columns, " << grid_rows << " rows, sample 4,7: " << binary_map[4][7] << std::endl;
		
	}



	void printBinaryMap() {
		for (int y = 0; y < grid_rows; y++)
		{
			for (int x = 0; x < grid_columns; x++)
				if (binary_map[x][y] == -1)
					std::cout << " ";
				else
					std::cout << binary_map[x][y];
			std::cout << std::endl;
		}
	}


	void printBinaryMapRows() {
		for (int x = 0; x < grid_columns; x++)
		{
			int count_numbers_rows = 0;
			int count_non_numbers_rows = 0;
			for (int y = 0; y < grid_rows; y++)
			{
				if (binary_map[x][y] != -1)
				{
					count_numbers_rows += binary_map[x][y];
					count_non_numbers_rows += 1 - binary_map[x][y];
				}
			}
			std::cout << "Column[" << x << "]: " << count_numbers_rows << " / " << count_non_numbers_rows << " - % = " << static_cast<float>(count_numbers_rows) / (count_non_numbers_rows + count_numbers_rows) << std::endl;
		}
	}

	void printBinaryMapColumns() {
		for (int y = 0; y < grid_rows; y++)
		{
			int count_numbers_columns = 0;
			int count_non_numbers_columns = 0;
			for (int x = 0; x < grid_columns; x++)
			{
				if (binary_map[x][y] != -1)
				{
					count_numbers_columns += binary_map[x][y];
					count_non_numbers_columns += 1 - binary_map[x][y];
				}
			}
			std::cout << "Row[" << y << "]: " << count_numbers_columns << " / " << count_non_numbers_columns << " - % = " << static_cast<float>(count_numbers_columns) / (count_numbers_columns + count_non_numbers_columns) << std::endl;
		}
	}




};

typedef std::vector <std::string> ocr_output_t;
typedef std::vector <float> ocr_output_confidence_t;
typedef std::vector <BoxPixel> ocr_box_t;
typedef std::vector <TextType> ocr_text_type_t;




class OCROutput
{
private:
	ocr_output_t output;
	ocr_output_confidence_t output_confidence;
	ocr_box_t boxes;
	ocr_text_type_t text_type;
	tesseract::PageIteratorLevel level;

public:


	void setNewElement(std::string text, float confidence, int left, int top, int right, int bottom, tesseract::PageIteratorLevel page_iterator_level) {
		output.push_back(text);
		output_confidence.push_back(confidence);
		boxes.push_back({left, top, right, bottom});
		level = page_iterator_level;
	}


	void Init (tesseract::ResultIterator *ri) {

		do
		{
			tesseract::PageIteratorLevel current_page_iterator_level;
			if (ri->IsAtBeginningOf(tesseract::RIL_TEXTLINE)) {
				// New line is encountered
				current_page_iterator_level = tesseract::RIL_TEXTLINE;
				std::string line = ri->GetUTF8Text(current_page_iterator_level);

				float conf = ri->Confidence(current_page_iterator_level);
				int left, top, right, bottom;
				ri->BoundingBox(current_page_iterator_level, &left, &top, &right, &bottom);
			}
			if (ri->IsAtBeginningOf(tesseract::RIL_WORD)) {
				// New word is encountered
				current_page_iterator_level = tesseract::RIL_WORD;
				std::string word = ri->GetUTF8Text(current_page_iterator_level);
				float conf = ri->Confidence(current_page_iterator_level);
				int left, top, right, bottom;
				ri->BoundingBox(current_page_iterator_level, &left, &top, &right, &bottom);
				setNewElement(word, conf, left, top, right, bottom, current_page_iterator_level);
			}
		} while (ri->Next(tesseract::RIL_WORD));

	}


	void printRawPage() {
		for (int i = 0; i < output.size(); i++) {
			std::cout << output[i] << " @ confidence " << output_confidence[i] << ": Positions: " << boxes[i].getDimensions() << std::endl;
				;
		}
	}

	int getOCRHeight() {
		int height = 0;
		for (int i = 0; i < output.size(); i++)
		{
			if (height < boxes[i].bottom)
				height = boxes[i].bottom;
		}
		return height;
	}

	int getOCRWidth() {
		int width = 0;
		for (int i = 0; i < output.size(); i++)
		{
			if (width < boxes[i].right)
				width = boxes[i].right;
		}
		return width;
	}

	/*Should not be in this class*/
	bool is_number(const std::string &str)
	{
		if (str.find('0') != std::string::npos ||
			str.find('1') != std::string::npos ||
			str.find('2') != std::string::npos ||
			str.find('3') != std::string::npos ||
			str.find('4') != std::string::npos ||
			str.find('5') != std::string::npos ||
			str.find('6') != std::string::npos ||
			str.find('7') != std::string::npos ||
			str.find('8') != std::string::npos ||
			str.find('9') != std::string::npos )/*||
			str.find('-') != std::string::npos ||
			str.find('(') != std::string::npos ||
			str.find(')') != std::string::npos)*/
		{
			return true;
		}
		return false;
	}


	BoxPixel getGridBox(BinarizedMap binary_map, int width, int height) {

		
		int max_columns = binary_map.getGridColumns();
		int max_rows = binary_map.getGridRows();
		int increment = binary_map.getIncrement();
		//for (int i = grid_columns; i > 0; i++
		
		int new_left = 0;
		int new_top = 0;
		int new_right = width;
		int new_bottom = height;

		//Exclude title
		int x = max_columns;
		while (x > 0)
		{
			for (int y = 0; y < max_rows; y++)
			{
				if (1)
				{
					new_top = y * increment;
					break;
				}
			}
			x--;
		}
		//***Remove these hardcoded values
		new_left = 200;
		new_top = 250;
		return BoxPixel(new_left, new_top, new_right, new_bottom);
	}



	BoxPixel getDataBox(BinarizedMap binary_map, BoxPixel grid_box) {
		int new_left = grid_box.left;
		int new_top = grid_box.top;
		int new_right = grid_box.right;
		int new_bottom = grid_box.bottom;

		int increment = binary_map.getIncrement();
		int grid_bottom_row = grid_box.bottom / increment;
		int grid_top_row = grid_box.top / increment;
		int grid_left_column = grid_box.left / increment;
		int grid_right_column = grid_box.right / increment;

		std::cout << grid_right_column << " " << grid_left_column;

		float *is_number_percentage_column = new float[grid_right_column - grid_left_column];
		for (int i = 0; i < grid_right_column - grid_left_column; i++)
			is_number_percentage_column[i] = 0.0;


		//For each column in the grid box, get number percentage
		for (int x = 0; x < grid_right_column - grid_left_column; x++)
		{
			int count_with_text_column = 0;
			float is_number_percentage = 0.0;			
			for (int y = grid_top_row; y < grid_bottom_row; y++)
			{
				if (binary_map.getBinaryMapPoint(x, y) != -1)
				{
					is_number_percentage += binary_map.getBinaryMapPoint(x, y);
					count_with_text_column++;
				}
			}
			is_number_percentage_column[x] = is_number_percentage / count_with_text_column;
			std::cout << "Is Number % column[" << x << "] = " << is_number_percentage_column[x] << std::endl;
			std::cout << "Total columns looked: " << grid_right_column - grid_left_column << " - Total columns with text: " << count_with_text_column << std::endl;
		}

		

		//get overall average of numbers in % in grid box
		float is_number_percentage_average = 0.0;
		for (int i = 0; i < grid_right_column - grid_left_column; i++)
		{
			if (!isnan(is_number_percentage_column[i]))
			{
				std::cout << "Adding " << is_number_percentage_column[i] << " to " << is_number_percentage_average << std::endl;
				is_number_percentage_average += is_number_percentage_column[i];
			}
			
		}
		std::cout << "Before: is_number_percentage_average = " << is_number_percentage_average << " - grid: " << (grid_right_column - grid_left_column);
		is_number_percentage_average = is_number_percentage_average / (grid_right_column - grid_left_column);
		std::cout << "Is Number % average = " << is_number_percentage_average << " - number of X looked at: " << (grid_right_column - grid_left_column) << std::endl;
		

		//evaluate data box left
		for (int i = 0; i < grid_right_column - grid_left_column; i++)
		{
			if (is_number_percentage_column[i] < (is_number_percentage_average / 2) && is_number_percentage_column[i] > 0)
			{
				new_left = i * increment;
				std::cout << "is_number_percentage_column[" << i << "] = " << is_number_percentage_column[i] << " - is_number_percentage_average = " << is_number_percentage_average << " new_left = " << increment << " * " << i << " = " << increment*i << "( = " << new_left << ")" << std::endl;
			}
				
		}
		std::cout << "New left = " << new_left << std::endl;


		//evaluate data box top





		//***Remove hardcoded values in setGridBox()
		return BoxPixel(new_left, new_top, new_right, new_bottom);
	}


	void setDataBox() {
		
		int increment_value = 10;
		int grid_columns = getOCRWidth() / increment_value;
		int grid_rows = getOCRHeight() / increment_value;

		BinarizedMap binary_map = BinarizedMap(grid_rows, grid_columns, increment_value);

				
		binary_map.load_binary_map(output.size(), boxes, output);

		


		binary_map.printBinaryMap();
		system("pause");
		
		//binary_map.printBinaryMapColumns();
		//system("pause");

		//binary_map.printBinaryMapRows();
		//system("pause");


			
		//BoxPixel grid_box = BoxPixel(0, 0, getOCRWidth(), getOCRHeight());
		BoxPixel grid_box = getGridBox(binary_map, getOCRWidth(), getOCRHeight());
		BoxPixel data_box = getDataBox(binary_map, grid_box);


		std::cout << "Grid box: " << grid_box.getDimensions() << std::endl;
		std::cout << "Data box: " << data_box.getDimensions() << std::endl;
		system("pause");


					
		

		


	}




	void getTextType() {
		std::cout << "Text type: " << text_type[0] << std::endl;
	}

	//******************* Private functions *********************
	void setTextType(TextType text_type, int position) {
		if (this->text_type.size() <= position)
			this->text_type.push_back(text_type);
		else
			this->text_type[position] = text_type;
	}
};




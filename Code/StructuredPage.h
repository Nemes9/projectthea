#pragma once

#ifndef ADD_VECTOR
#define ADD_VECTOR
#include <vector>
#endif





typedef std::vector <int> array_t;
typedef std::vector <array_t> matrix_t;

class StructuredPage
{
private:
	std::vector<std::string> column_label;
	std::vector<std::string> row_label;
	matrix_t matrix;

public:
	StructuredPage()
	{
		column_label = (std::vector<std::string>(1, ""));
		row_label = (std::vector<std::string>(1, ""));
		matrix = { { 1 },{ 2, 2, 2 },{ 3, 4, 5, 6 } };
		//matrix = { {0} };
	}


	std::string getColumnLabel()
	{
		std::string str;
		str.append("Number of columns: ");
		str.append(std::to_string(column_label.size()));
		str.append("\n");
		for (int i = 0; i<column_label.size(); ++i) {
			str.append("Column header ");
			str.append(std::to_string(i));
			str.append(": ");
			str.append(column_label[i]);
			str.append("\n");
		}
		return str;
	}

	std::string getRowLabel()
	{
		std::string str;
		str.append("Number of rows: ");
		str.append(std::to_string(row_label.size()));
		str.append("\n");
		for (int i = 0; i<row_label.size(); ++i) {
			str.append("Row header ");
			str.append(std::to_string(i));
			str.append(": ");
			str.append(row_label[i]);
			str.append("\n");
		}
		return str;
	}

	std::string getMatrix()
	{
		std::string str;
		str.append("Matrix: ");
		str.append(std::to_string(matrix.size()));
		str.append(" by ");
		str.append(std::to_string(matrix[0].size()));
		str.append("\n");
		for (int row = 0; row<matrix.size(); ++row) {
			for (int column = 0; column < matrix[row].size(); ++column) {
				str.append(std::to_string(matrix[row][column]));
				if (column + 1 < matrix[row].size()) {
					str.append(" | ");
				}
			}
			str.append("\n");
		}
		str.append("\n");
		return str;
	}



	void printColumnLabel()
	{
		std::cout << getColumnLabel();
	}

	void printRowLabel()
	{
		std::cout << getRowLabel();
	}

	void printMatrix()
	{
		std::cout << getMatrix();
	}

	void addColumnLabel(std::string str) {
		column_label.push_back(str);
	}

	void addRowLabel(std::string str) {
		row_label.push_back(str);
	}

	void addMatrixElement(int row, int value) {
		if (row >= matrix.size()) {
			array_t new_row = { value };
			matrix.push_back(new_row);
		}
		else {
			matrix[row].push_back(value);
		}
	}


	void setColumnLabel(int position, std::string str)
	{
		if (position >= column_label.size()) {
			resizeWidth();

		}
		column_label[position] = str;
	}


	void setRowLabel(int position, std::string str)
	{

		if (position >= row_label.size()) {
			resizeHeight();
		}
		row_label[position] = str;
	}

	void setMatrix(int row, int column, int value)
	{
		while (row > row_label.size()) {
			resizeHeight();
		}
		while (column > column_label.size()) {
			resizeWidth();
		}
		matrix[row][column] = value;
	}


	//******************* Private functions *********************
	void resizeHeight()
	{
		int newSize = row_label.size() + 1;
		resizeHeight(newSize);
	}
	void resizeHeight(int height)
	{
		matrix.resize(height);
		row_label.resize(height);
	}

	void resizeWidth()
	{
		int newSize = column_label.size() + 1;
		resizeWidth(newSize);
	}
	void resizeWidth(int width)
	{
		for (int row = 0; row < matrix.size(); row++) {
			matrix[row].resize(width);
		}
		column_label.resize(width);
	}


};


